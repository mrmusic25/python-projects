#!/usr/bin/env python3
#
# Mailer.py - A program to handle sending and receiving of emails
#
# Changelog:
# v0.1.1
# - After testing, it is better to do ONLY html or txt message
# - Doing both causes the second one to be added as an attachment for some reason
# - Program will now prefer HTML over message, and raise an exception if both are empty
#
# v0.1.0
# - Added ability to send HTML with messages in addition to plaintext
# - If only HTML given, sets a default plaintext message as a backup
# - HTML only added as a multipart if it is detected.
# - Message is now an optional argument, as is HTML, which changes the API
# - send() will now raise an exception if both message and HTML are empty
# - Fixed a typo preventing bcc from being utilized in send()
#
# v0.0.8
# - Changed from gnome-keyring to keyrings.cryptfile for ease of use
#
# v0.0.7
# - send() will now explicitly close the SMTP connection each time
#
# v0.0.6
# - Added ability to set send name for emails
# - If left empty, emails will be sent normally without name
#
# v0.0.5
# - Changed import statements to correctly bring in MIME
# - Fixed various runtime issues from testing
#
# v0.0.4
# - Wrote send()
# - Python can import Mailer with no issues, but don't have server setup to test it with yet
#
# v0.0.3
# - Finished writing connect()
# - Added getPass() to handle getting a password
# - ssl, keyring, getpass, and poplib added to the list or imports
# - Added exception handling to everything to connect()
#
# v0.0.2
# - Added isValid() to raise exceptions if settings are invalid
# - Constructor now supports 'user@domain' type inputs
#
# v0.0.1
# - Initial commit
#
# Author: Kyle Krattiger (gitlab.com/mrmusic25)
#
# License: This program is licensed under GNU General Public License 3.0
# 
# v0.1.1, 24 Mar. 2022, 17:08 PDT

### Imports

import smtplib
import imaplib
import poplib
import os
import ssl
from email.mime.multipart import MIMEMultipart
from email.mime.base import MIMEBase
from email.mime.text import MIMEText
from email.utils import formataddr
from email.header import Header
from email import encoders
import getpass
#import keyring
from keyrings.cryptfile.cryptfile import CryptFileKeyring

### Class

class Mailer:
    ### Variables
    mailUser = str()        # Username for the email service
    mailDomain = str()      # Domain of the email service
    mailServer = str()      # Optional if it is needed for connections
    mailName = str()        # Optional custom name for sending emails
    keyringPass = None      # Optional string for the keyring unlock password, otherwise prompts user for login
    sendPort = 587          # Port to use for SMTP/SMTPS
    sendMode = 'submission' # Mode to use for sending (SMTP, SMTPS, SUBMISSION)
    recvPort = 143          # Port to use with IMAP/POP3
    recvMode = 'imap'       # Mode to use for receiving (IMAP or POP3)
    interactive = False     # Tells program whether or not to interactively have user enter password for key
    
    ### Constructor
    # Initialize object. user can be just the username, or 'user@domain' style
    def __init__(self, user: str, domain=str(), server=str(), sendmode=str(), sendport=0, recvmode=str(), name=str(), recvport=0, keypass=None, interactive=False):
        if user.find('@') != -1:
            u = user.split('@')
            self.mailUser = u[0]
            self.mailDomain = u[1]
        else:
            self.mailUser = user
        
        if domain:
            self.mailDomain = domain
        if server:
            self.mailServer = server
        if sendmode:
            self.sendMode = sendmode.lower()
        if sendport > 0:
            self.sendPort = sendport
        if recvmode:
            self.recvMode = recvmode.lower()
        if recvport > 0:
            self.recvPort = recvport
        if name:
            self.mailName = name
        if keypass:
            self.keyringPass = keypass
        if interactive:
            self.interactive = True

    ### Functions
    # Checks to see if the set options are valid, throws an error if not
    # Setting send to True checks sending ability, false checks receiving
    # Setting both to true will check both sending and receiving
    def isValid(self, send=False, both=False):
        # Check mailUser
        if not self.mailUser:
            raise Exception('Mail user is not set')
        if self.mailUser.find('@') != -1:
            raise Exception('Mail user ' + str(self.mailUser) + " is in the wrong format!")

        # Check mailDomain
        if not self.mailDomain:
            raise Exception('Mail domain is not set')
        
        # Check sendMode and sendPort
        if send is True or both is True:
            if self.sendMode != 'smtp' and self.sendMode != 'smtps' and self.sendMode != 'submission':
                raise Exception('Send mode ' + str(self.sendMode) + ' is not a valid option!')
            if not isinstance(self.sendPort,int) or self.sendPort <= 0:
                raise Exception('Send port ' + str(self.sendPort) + ' is not a valid number!')

        # Check recvMode and recvPort
        if send is False or both is True:
            if self.recvMode != 'imap' and self.recvMode != 'pop3':
                raise Exception('Receive mode ' + str(self.recvMode) + ' is not vaild!')
            if not isinstance(self.recvPort,int) or self.recvPort <= 0:
                raise Exception('Receive port ' + str(self.recvPort) + ' is not a valid number!')
        
        # Everything is good if it makes it this far
        return True

    # Returns fully formed email address
    def getEmail(self):
        return str(self.mailUser + '@' + self.mailDomain)
    
    # Asks user for a password until two passwords match. Returns a string of the password once they match
    def getPass(self, mode='email'):
        p1 = "b"
        p2 = "a"

        while p1 != p2:
            p1 = getpass.getpass(prompt=str("Please enter the password for " + mode + " and press [ENTER]: "))
            p2 = getpass.getpass(prompt=str("Please re-enter password again for confirmation and press [ENTER]: "))
            if p1 != p2:
                print("ERROR: Passwords do not match! Please try again!")

        return p1

    # Automate setting up password with cryptkey
    def setupKeyring( self ):
        if not self.interactive:
            raise Exception("Interactive mode not enabled but setupKeyring was called! Silently failing...")

        # Attempt to import the keyring
        key = CryptFileKeyring()
        if self.keyringPass is not None:
            key.keyring_key = self.keyringPass # Login if password available, otherwise user is in interactive mode and can enter it now

        pw = self.getPass()
        key.set_password(str('email'),self.getEmail(),pw)

        return True

    # Attempt to connect to the server and return the session
    # If send is True, opens a session for sending mail. False, receiving.
    def connect(self, send=False):
        if send is True and self.isValid(send=True) is True:
            # Prepare SSL context if needed
            if self.sendMode == 'submission' or self.sendMode == 'smtps':
                context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
            else:
                context = None

            # Setup session based on if mailServer of sendPort is not empty
            if self.sendPort > 0 and self.mailServer:
                session = smtplib.SMTP(host=self.mailServer,port=self.sendPort)
            elif self.mailServer:
                session = smtplib.SMTP(host=self.mailServer)
            elif self.sendPort > 0:
                session = smtplib.SMTP(host=self.mailDomain,port=self.sendPort)
            else:
                session = smtplib.SMTP(host=self.mailDomain)

            # Special step for SMTP: startTLS if necessary
            if context is not None:
                session.ehlo()
                session.starttls(context=context)
            session.ehlo()

        # Receive mode
        elif self.isValid() is True:
            if self.recvMode == 'imap':
                if self.recvPort > 0 and self.mailServer:
                    session = imaplib.IMAP4_SSL(host=self.mailServer,port=self.recvPort)
                elif self.mailServer:
                    session = imaplib.IMAP4_SSL(host=self.mailServer)
                elif self.recvPort > 0:
                    session = imaplib.IMAP4_SSL(host=self.mailDomain,port=self.recvPort)
                else:
                    session = imaplib.IMAP4_SSL(host=self.mailDomain)
            else:
                if self.recvPort > 0 and self.mailServer:
                    session = poplib.POP3_SSL(host=self.mailServer,port=self.recvPort)
                elif self.mailServer:
                    session = poplib.POP3_SSL(host=self.mailServer)
                elif self.recvPort > 0:
                    session = poplib.POP3_SSL(host=self.mailDomain,port=self.recvPort)
                else:
                    session = poplib.POP3_SSL(host=self.mailDomain)
        else:
            raise Exception("Could not create a working session!")

        # Get the password for 'email_<mode>' from the keyring, and set it if not available
        mailPass = str()
        k = CryptFileKeyring()
        if self.keyringPass is not None:
            k.keyring_key = self.keyringPass
        
        try:
            mailPass = k.get_password(str('email'),self.getEmail())
        except:
            raise Exception("Unable to get password from cryptfile!")
        """
        while not mailPass:
            try:
                k = keyring.get_keyring()
                mailPass = k.get_password(str("email"), self.getEmail())
            except keyring.errors.NoKeyringError:
                raise Exception("No keyring backend detected! Please install one from https://pypi.org/project/keyring/ !")
            except keyring.errors.InitError:
                raise Exception("Keyring could not be initialized! Please make sure a keyring from https://pypi.org/project/keyring/ is installed!")
            except keyring.errors.KeyringLocked:
                raise Exception("Current user does not have access to keyring and could not unlock it. Please fix and re-run!")
            except keyring.errors.KeyringError:
                # User/service combo not found in keyring, set password and retry
                print("Password is not set for " + self.getEmail() + "! Please follow prompts to set password!")
                p = self.getPass()
                try:
                    k.set_password(str("email"), self.getEmail(),p)
                except keyring.errors.PasswordSetError:
                    raise Exception("Password could not be stored in keyring, does current user have permission? Please fix and re-run!")
                p = str()
                mailPass = str()
        """
        # Now, attempt to login with credentials
        try:
            if isinstance(session,poplib.POP3_SSL):
                session.user(self.getEmail())
                session.pass_(mailPass)
            else:
                session.login(self.getEmail(),mailPass)

        except imaplib.IMAP4_SSL.error:
            raise Exception(str("Failed to connect to IMAP4_SSL session; email=" + self.getEmail() + ", server=" + self.mailServer + ", recvPort=" + str(self.recvPort)))
        except poplib.error_proto:
            raise Exception(str("Failed to connect to POP3_SSL session; email=" + self.getEmail() + ", server=" + self.mailServer + ", recvPort=" + str(self.recvPort)))
        except smtplib.SMTPDataError:
            raise Exception(str("Failed to connect to SMTP session; email=" + self.getEmail() + ", server=" + self.mailServer + ", sendPort=" + str(self.sendPort)))
        except smtplib.SMTPDataError:
            raise Exception(str("Failed to connect to SMTP_SSL session; email=" + self.getEmail() + ", server=" + self.mailServer + ", sendPort=" + str(self.sendPort)))

        # If you made it this far, session successfully connected. Return the session for use
        return session

    # Converts a delimited string to a list (comma by default, but can be specified)
    def strtolist(self, s: str, delim=str(',')):
        return s.split(sep=delim)

    # Convert a list to a str
    # Adds delimiter at end of each line based on 
    def listtostr(self, l: list, delim='\n'):
        s = str()
        once = False
        for item in l:
            if once is False:
                s = str(item)
                once = True
            else:
                s = s + str(delim) + str(item)
        
        return s

    # Attempts to send given message to given address
    # If "to" is just a string, will attempt to convert to a list of recipients. Same for cc and bcc
    def send(self, to: list, message = "", html = "", cc=None, bcc = None, subject="RE", attachments=None):
        # Format "to" as a list
        toList = list()
        if isinstance(to,str):
            toList = self.strtolist(to)
        elif isinstance(to,list):
            for x in to:
                if not isinstance(x,str):
                    raise TypeError
            toList = to

        # Same for cc
        ccList = list()
        if cc is not None:
            if isinstance(cc,str):
                ccList = self.strtolist(cc)
            elif isinstance(cc,list):
                for x in cc:
                    if not isinstance(x,str):
                        raise TypeError
                ccList = cc
        
        # Lastly, check bcc
        bccList = list()
        if bcc is not None:
            if isinstance(bcc,str):
                bccList = self.strtolist(bcc)
            elif isinstance(bcc,list):
                for x in bcc:
                    if not isinstance(x,str):
                        raise TypeError
                bccList = bcc

        # Now, prepare the email for sending
        # Start by making sure message and HTML are strings
        if isinstance(html,list):
            sendHTML = self.listtostr(html)
        elif html != "":
            sendHTML = html
        else:
            sendHTML = ""
        
        if isinstance(message,list):
            sendMsg = self.listtostr(message)
        elif message != "":
            sendMsg = message
        else:
            sendMsg = ""

        if sendHTML != "" and sendMsg == "":
            sendMsg = "No alternative plaintext provided, please open this message in a browser to view HTML message!\n\n    -- Mailer.py"
        elif sendHTML == "" and sendMsg == "":
            raise Exception("Both HTML and message were empty for Mailer.py::send(), unable to continue! Please provide at least one!")
        
        msg = MIMEMultipart()
        
        if self.mailName:
            msg['From'] = formataddr((str(Header(self.mailName,'utf-8')),self.getEmail()))
        else:
            msg['From'] = self.getEmail()
        
        msg['To'] = self.listtostr(toList,delim=", ")
        msg['Subject'] = subject
        if not ccList:
            msg['Cc'] = self.listtostr(ccList,delim=", ")
        if not bccList:
            msg['Bcc'] = self.listtostr(bccList,delim=", ")

        # Attach correct part, or raise exception if unable
        if sendHTML != "":
            msg.attach(MIMEText(sendHTML,"html"))
        elif sendMsg != "":
            msg.attach(MIMEText(sendMsg,"plain"))
        else:
            raise Exception("Neither html nor message was set, no data to send!")

        # Add any attachments to the email
        # Based of tutorial: https://realpython.com/python-send-email/#adding-attachments-using-the-email-package
        if attachments is not None:
            if isinstance(attachments,list):
                for f in attachments:
                    n = os.path.basename(str(f)) # Converting from a string here allows the attachments to be a PurePath or similar
                    with open(str(f),"rb") as attachment:
                        part = MIMEBase("application", "octet-stream")
                        part.set_payload(attachment.read())
                    encoders.encode_base64(part)
                    part.add_header("Content-Disposition",f"attachment; filename= {n}",)
                    msg.attach(part)
            else:
                n = os.path.basename(str(attachments)) # Converting from a string here allows the attachments to be a PurePath or similar
                with open(str(attachments),"rb") as attachment:
                    part = MIMEBase("application", "octet-stream")
                    part.set_payload(attachment.read())
                encoders.encode_base64(part)
                part.add_header("Content-Disposition",f"attachment; filename= {n}",)
                msg.attach(part)
                    
        # Everything is now ready to send. Procure a session, and send!
        ses = self.connect(send=True)
        text = msg.as_string()

        # Not in a try-catch so that errors are recorded outside of class
        # Maybe catch errors here in future? It will just raise a new error anyways, so perhaps not.
        ses.sendmail(self.getEmail(),self.listtostr(toList,delim=", "),text)

        # Finally done! Return True if it gets this far
        ses.quit()
        return True

    # Returns a List of email.Message objects from the specified mailbox
    def get(self, unreadOnly=True, mailbox='INBOX'):
        # First, make sure settings are valid. Exception will be raised if invalid settings detected
        session = self.connect()

        msgList = list()