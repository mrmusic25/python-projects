#!/usr/bin/env python3
#
# messenger.py - Simple script to send a notification email using Mailer
#
# Usage:
#   ./messenger.py [options] <"message">
#      See './messenger.py --help' (or below) for options
#      For best results, wrap the entire message in quotation marks
#      For first time run, I recommend './messenger.py -u <user> -k <passphrase> --setup', along with other options you may need
# 
# Author: Kyle Krattiger (gitlab.com/mrmusic25)
#
# License: This program is licensed under GNU General Public License 3.0
#
# Changelog:
# v0.1.1
# - Updated send() call to use new version of Mailer.send()
#
# v0.1.0
# - Initial version
#
# v0.1.1, 29 Apr. 2022 17:10 PDT

### Imports

import re
import argparse
import configparser
import pathlib
from sys import exit
from getpass import getpass
from platform import node
import Logger
import Mailer

### Variables

HOME = pathlib.Path().home()                 # User's home directory, used for storing log and config
LOGFILE = HOME.joinpath('messenger.log')     # Log file for Logger
CONFFILE = HOME.joinpath('.messenger.ini')   # Location of the configuration file for this user
PASSFILE = HOME.joinpath('.pyCryptKeyring')  # Location of the file where the keyring password is kept
log = Logger.Logger(str(LOGFILE))            # Object containing the Logger
mailUser = ""                                # Email address to send message from
mailServer = ""                              # Server to use for sending, if needed. (No default)
mailPort = 25                                # Port used for connecting to SMTP/SMTPS/Submission
sendMode = 'smtps'                           # Email send protocol that Mailer accepts. Can be either 'smtp', 'smtps', or 'submission'
passphrase = ""                              # Where the passphrase for the keyring will be stored (no default)
toList = []                                  # List of email addresses to send message to
subject = "New Message from " + node() + "!" # Subject used for sending email
message = ""                                 # Content of message that will be sent to toList
setupMode = False                            # Whether or not to setup a new user interactively

### Functions

# Saves both config and passphrase
def saveConfig():
    # First, save the config file
    conf = configparser.ConfigParser()
    conf['Global'] = { 'mailUser': mailUser,
                       'mailServer': mailServer,
                       'mailPort': str(mailPort),
                       'sendMode': sendMode,
                       'subject': subject,
                       'recipients': asStr(toList) }
    with open(str(CONFFILE), 'w') as f:
        conf.write(f)
        f.close()
    
    # Then, save the passphrase
    with open(str(PASSFILE), 'w') as f:
        f.write(passphrase)
        f.close()

    return True

# Load the passphrase from the file. Returns false if file is missing or is empty, true on success
def loadPassphrase():
    # Make sure file exists before opening
    if not pathlib.Path(PASSFILE).exists():
        log.log("Passphrase file " + str(PASSFILE) + " does not exist, user must set it up!",2)
        return False

    with open(str(PASSFILE),'r') as f:
        content = f.read().rstrip()
        f.close()

    # Log if file is empty, other wise set passphrase and return
    if content == "":
        log.log("Passphrase file " + str(PASSFILE) + " was empty, needs to be setup!",3)
        return False
    else:
        global passphrase
        passphrase = content
        return True

# Loads config with configparser
def loadConfig():
    # Globals that will be set
    global mailUser, mailServer, mailPort, sendMode, subject, toList

    # Read in the config, if the file exists
    if not pathlib.Path(CONFFILE).exists():
        log.log("Config file " + str(CONFFILE) + " does not exist, nothing to import!",2)
    else:
        conf = configparser.ConfigParser()
        conf.read(str(CONFFILE))
        mailUser = conf['Global']['mailUser']
        mailServer = conf['Global']['mailServer']
        mailPort = int(conf['Global']['mailPort'])
        sendMode = conf['Global']['sendMode']
        subject = conf['Global']['subject']
        toList = asList(conf['Global']['recipients'])

    # Finally, load passphrase from file
    loadPassphrase()

    return True


# Splits a string by the deliminator
def asList( s: str, delim = ',' ):
    return s.split(delim)

# Returns a string of a list delimited by given character
def asStr( l, delim = ',' ):
    s = ""
    once = False
    for i in l:
        if once:
            s += delim + str(i)
        else:
            s += str(i)
            once = True
    return s

# Setup the passphrase and CryptKey if needed
def setup():
    # First make sure passphrase is setup
    global passphrase
    if not passphrase:
        a = "x"
        b = "c"
        log.log("User must enter the desired/existing passphrase for their keyring! Please use a secure password, it will later be stored in: " + str(PASSFILE),2)
        while (a != b):
            a = getpass('Please enter the passphrase you wish to use for you keyring then press [ENTER]: ')
            b = getpass('Re-enter the same password then press [ENTER]: ')

        passphrase = a

    log.log("Passphrase is setup, handing setup over to Mailer wher you will enter password for mail user: " + mailUser,2)
    mail = Mailer.Mailer(mailUser,keypass=passphrase,interactive=setupMode)
    mail.setupKeyring()

    # Done
    log.log("Done setting up passphrase and keyring!",1)
    return True

### Main

if __name__ == "__main__":
   
    ## Setup arguments and parser
    parser = argparse.ArgumentParser(description="Simple program for sending status messages over email")
    parser.add_argument('-f','--key-file',dest='keyfile',help="File that contains the password for the keyring. If file does not exist program will interactively ask for keys/passwords as needed. (Default: '$HOME/.pyCryptKeyring'",type=str,default=None)
    parser.add_argument('-u','--user',help="Username used for logging into the email server.",type=str,default=None)
    parser.add_argument('-c','--config-file',dest='configfile',help="Location of the config file. Default is '$HOME/.messenger.ini'. Gets loaded first, other options will override config and then be saved!",type=str,default=None)
    parser.add_argument('-k','--key',help="Password for the keyring containing email credentials. Stored in the file given with -f|--key-file.",type=str,default=None)
    parser.add_argument('-s','--server',help="Server to use for sending emails (if it is different than the domain). If empty, uses the domain after the @.",type=str,default=None)
    parser.add_argument('-m','--send-mode',dest="sendmode",help="Mode to use for sending emails. Acceptable options: smtp, smtps, submission",type=str,default=None)
    parser.add_argument('-p','--port',help="Port number used to connect to SMTP/SMTPS/Submission. Default is 25.",type=int,default=None)
    parser.add_argument('-r','--recipients',help="Comma-delimited list of users to send email to. (e.g. 'kyle@me.com', or 'kyle@me.com,mike@me.com,ash@me.com'",type=str,default=None)
    parser.add_argument('-b','--subject',help="Sets the subject of the email. 'New Message from @hostname!' by default.",type=str,default=None) # TODO: Add @@ values that can be input with regex. Maybe similar for message?
    parser.add_argument('-v','--verbose',help="Enables verbose logging",action='store_true',default=None)
    parser.add_argument('--setup',help="Enables interactive mode for setting up a new mailing system for current user.",action='store_true',default=None)
    parser.add_argument('message',help="Required. Message you want sent to each recipient. Enclose in quotation marks or whole message may not send.",type=str)

    # Parse and import the arguments
    try:
        args = parser.parse_args()
    except argparse.ArgumentError as e:
        log.log("Could not parse arguments! Exiting... Args: " + asStr(e.args),4)
        parser.print_help()
        exit(1)

    # Verbose first
    if args.verbose:
        log.setPrintLevel(1)
        log.log("Verbose logging enabled!")

    # Load old config and key-file in case they need to be overwritten
    if args.keyfile:
        if pathlib.Path(args.keyfile).is_absolute():
            PASSFILE = pathlib.Path(args.keyfile)
        else:
            log.log("Given key file " + args.keyfile + " is not absolute, storing in " + str(HOME),2)
            PASSFILE = HOME.joinpath(args.keyfile)
        
        log.log("Key file is now: " + str(PASSFILE),1)
        loadPassphrase()

    if args.configfile:
        if pathlib.Path(args.configfile).is_absolute():
            CONFFILE = pathlib.Path(args.configfile)
        else:
            log.log("Given config file " + args.configfile + " is not absolute, storing in " + str(HOME),2)
            CONFFILE = HOME.joinpath(args.configfile)
        log.log("Config file is now: " + str(CONFFILE),1)
        
    loadConfig()

    # Now, overwrite any old options with user options
    if args.user:
        if not re.match(r'.*@.*\.[a-zA-Z0-9]{2,}',args.user):
            log.log("Given username is not a properly formatted email address! Please fix and re-run!",4)
            exit(1)
        else:
            mailUser = args.user
            log.log("Mail user is now: " + mailUser,1)

    if args.key:
        passphrase = args.key
        log.log("Passphrase given as an argument, will be saved to keyfile " + str(PASSFILE),1)

    if args.server:
        mailServer = args.server
        log.log("Mail server is now set to " + mailServer,1)

    if args.sendmode:
        if str(args.sendmode).lower() in ('smtp','smtps','submission'):
            sendMode = str(args.sendmode).lower()
            log.log("Send mode is now: " + sendMode,1)
        else:
            log.log("Given send mode " + args.sendmode + " is invalid! Please fix and re-run!",4)
            exit(1)

    if args.port:
        if int(args.port):
            mailPort = int(args.port)
            log.log("Mail port is now: " + str(mailPort),1)
        else:
            log.log("Given port " + str(args.port) + " is not a number! Please fix and re-run!",4)
            exit(1)

    if args.recipients:
        toList = asList(args.recipients)
        log.log("toList has been set, will be verified later!",1)

    if args.subject:
        subject = args.subject
        log.log("Subject is now: " + subject,1)

    if args.setup:
        setupMode = True
        log.log("Setup mode enabled, user will be asked to interact with program!",2)

    if not args.message:
        log.log("Main argument message was not given! Please fix and re-run!",4)
        exit(1)
    else:
        message = args.message
        log.log("Message has been set!",1)

    # All options input. Now, verify them all, that we have enough info to run, and save configs
    if not mailUser or not toList:
        log.log("Program does not have enough info to run! Mail user and recipient list must be set! Please fix and re-run!",4)
        exit(1)

    if passphrase == "" and not setupMode:
        log.log("Passphrase is not set and interactive (setup) mode is disabled! Please enable interactive mode and re-run for first time setup!",4)
        exit(1)
    elif setupMode:
        log.log("Entering setup mode for both email and passphrase!")
        setup()

    log.log("Saving configs before sending message...",1)
    saveConfig()

    # Finally, send the message with all the given options
    try:
        acc = Mailer.Mailer(mailUser,server=mailServer,sendmode=sendMode,sendport=mailPort,keypass=passphrase,interactive=setupMode)
        acc.send(toList,message=message,subject=subject)
    except Exception as e:
        log.log("Unable to send message! Args: " + asStr(e.args),4)
        exit(1)

    log.log("Successfully sent message " + message + " to user(s) " + asStr(toList),1)
    exit(0)

#EOF