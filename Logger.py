#!/usr/bin/env python
#
# logger.py - A remake of my log() function for Python
#
# Changelog:
# v0.1.2
# - Fixed newline output to be more consistent
# - Switched to using time.time() for seconds counting, just like commonFunctions.sh
# - Added a secondeConverter() function to return a string with the given seconds
# - Using pathlib to correctly touch new files for printing
# - Now tested and ready to go!
#
# v0.1.1
# - Forgot to add check for isInit and initLog()
# - Fixed some issues with time
#
# v0.1.0
# - Initial version
#
# Author: Kyle Krattiger (gitlab.com/mrmusic25/python-projects)
#
# Licensed under GNU General Public License 3.0
#
# v0.1.2, 14 Mar. 2021, 20:48 PDT

import os
import datetime
import time
import random
from pathlib import Path

## Levels
# These are the log levels. Both logLevel and printLevel use this to print that level and above.
#  e.g. If printLevel = 2, only 2 and above will print to the terminal.
# 0 - DBG, Debug messages
# 1 - INFO, Information events
# 2 - WARN, Warning message
# 3 - ERROR, Error messages
# 4 - FATAL Fatal messages

class Logger:
    ### Vars
    logFile = None    # Location of the log file
    logLevel = 1       # Level of messages printed to log file (default INFO)
    printLevel = 2     # Level printed to console (WARN default)
    defLevel = 1       # Default log level is no level is specified
    isInit = False     # Bool to tell whether log has been initialized on first run
    isLocked = False   # Locks the output file for writing, useful for multi-threaded applications
    startTime = None   # Time this object was initialized
    levels = [ "DBG", "INFO", "WARN", "ERROR", "FATAL" ]

    ### Constructor
    def __init__(self, lFile: str):
        self.logFile = lFile
        self.startTime = time.time()
     
    ### Functions
    # Returns a string of seconds, minutes, hours, and days
    def secondsConverter(self, secs: int):
        # Get vars ready
        seconds = secs
        minutes = 0
        hours = 0
        days = 0
        
        # Math time!
        while seconds >= 60:
            seconds -= 60
            minutes += 1
        while minutes >= 60:
            minutes -= 60
            hours += 1
        while hours >= 24:
            hours -= 24
            days += 1

        # Now, prepare the string
        s = ""
        if days > 0:
            s += str(int(days)).zfill(2) + ":"
        if hours > 0:
            s += str(int(hours)).zfill(2) + ":"
        s += str(int(minutes)).zfill(2) + ":" + str(int(seconds)).zfill(2)
        return s

    # Make sure the log can be written to, and write a time header to it
    def initLog(self):
        # First, make sure the path is writable
        self.lock()
        Path(self.logFile).touch(exist_ok=True)

        # Get the time and open the logfile
        time = datetime.datetime.now().strftime("%b. %d %y, %H:%M:%S %Z")
        try:
            f = open(self.logFile,"a")
        except:
            print("ERROR: Could not open log file " + self.logFile + "!", file=os.sys.stderr)
            return False

        # Write the initial string to the file
        try:
            f.write("\n***\n*** Log started at " + time + "\n***\n\n")
        except:
            print("Could not write starting string to " + self.logFile + "!", file=os.sys.stderr)
            f.close()
            return False
            
        # Close file and return
        f.close()
        self.isInit = True
        self.isLocked = False
        return True

    # Lock the file for use, and wait until it is free to write
    def lock(self):
        while self.isLocked == True:
            time.sleep(float(random.randint(1,20)/1000)) # 1-20 milliseconds for wait time
        
        self.isLocked = True
        return True

    # Change logging mode to debugging
    def debugMode(self):
        self.logLevel = 0
        self.printLevel = 0

    # Change print value
    def setPrintLevel(self,i: int):
        self.printLevel = i

    # Change log level
    def setLogLevel(self, i: int):
        self.logLevel = i

    # Change the log file
    def setLogFile(self, file: str):
        self.logFile = file
        self.isInit = False

    # Function that actually does the logging
    def log(self, message: str, level: int = None):
        # Prepare for writing
        if self.isInit == False:
            self.initLog()    
        self.lock()
        if level is None:
            currLevel = self.defLevel
        else:
            currLevel = level

        # Get the delta time for the message
        secs = time.time() - self.startTime

        # Prepare the string for writing
        timestr = self.secondsConverter(secs)

        s = str("[ " + timestr + " ] " + self.levels[currLevel] + ": " + message)

        # Now, write depending on levels
        if currLevel >= self.logLevel:
            try:
                f = open(self.logFile,"a")
                f.write(s + "\n")
                f.close()
            except:
                print("Could not write given message to log " + self.logFile + "!", file=os.sys.stderr)
        
        if currLevel >= self.printLevel:
            print(s)

        self.isLocked = False