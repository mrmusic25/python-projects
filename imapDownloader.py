#!/usr/bin/env python
#
# archiveDownloader.py - Checks an IMAP server for unread messages containing links and downloads them to given directory
#
# Changelog:
# v0.2.4
# - Changed program to use yt_dlp instead of youtube_dl, started working again instantly!
# - Updated format since yt_dlp is smart and mirrors description to title for tiktok!
#
# v0.2.3
# - Added -d|--dns-server option to hold over until Mailer is ready for use
#
# v0.2.2
# - Added a failure log where videos unable to be downloaded are sent
# - Will be stored in the current directory
#
# v0.2.1
# - Flag for sending messages was in the wrong direction
# - Despite that, I learned Verizon finally accepted my domain as legitimate!
# - Testing was otherwise successful though!
#
# v0.2.0
# - Added global option sendResponse to turn on/off responding to messages
# - Updated response() to confrom with this change
# - Changed the default ytdl output format based off tests
# - Added a temporary -p|--password option to pass to from command line for scritping purposes
# - This will be fixed once I figure out proper variable storage! Follow password safety practices with this!
# - Added option to turn on/off read-only mode with the IMAP server
#
# v0.1.3
# - Found a better, more reliable regex for finding links
# - Turns out I forgot to implement getLinks() this whole time. Fixed.
# - Program now functions exactly as intended now!
#
# v0.1.2
# - Implemented my Logger class
# - Added more debugging statements
# - Fixed more of the changes needed during debugging
# - More comments so I know what I'm looking at
# - Added options for both verbose and debug logging
#
# v0.1.1
# - Lots of debug lines added
# - Removed a lot of the list functionality for now
# - Implemented lots of changes to get this working
# - This version works, spamhaus is just being dumb and won't forward my emails now
#
# v0.1.0
# - Initial release version
# - Simple version that must be run by hand, saves nothing
#
# TODO:
# - Save options in file for reloading/automation
# - More options like ports, optional SSL, etc.
# - Whitelisting
# - Better exception handling
#   ~ Rebuild linux-pref/commonFunctions.sh::debug() in python?
# - More reply messages!
#  ~ Variety is better
#  ~ Find way to limit length of link? Find max SMS length first
#
# v0.2.4, 22 Mar. 2022, 19:00 PDT

### Imports

import imaplib
import getpass
import os
import email
import re
import argparse
import random
import ssl
import smtplib
from pathlib import Path
from pprint import pprint
import yt_dlp as youtube_dl
from Logger import Logger

### Variables

outputDir = str(Path.home())                              # Location where downloaded files will be dumped
ytdlFormat = str("%(upload_date)s %(title)s.%(ext)s")   # String given to -o for youtube-dl
imapUser = str()                                          # Username of IMAP user
imapPass = str()                                          # Password for IMAP
sendResponse = False                                      # Whether or not to respond to messages by default
readOnly = False                                          # Defines if inbox should be opened in read-only mode or not
serverAddress = str()                                     # FQDN of the email server
log = Logger("imapDownloader.log")                        # Log file location
failLog = str("imapDownloader-failures.txt")              # List of files that failed to download

### Functions

# Simple logging utility provided by youtube_dl
class SimpleLogger(object):
    def debug(self, msg):
        pass

    def warning(self, msg):
        print(msg,file=os.sys.stderr)

    def error(self, msg):
        print(msg,file=os.sys.stderr)

# Attempt to download the link given through youtube_dl
def ytdl(link, user):
    # Sanity check
    if link is None or link == "":
        log.log("Link is blank or missing, nothing for ytdl() to process! Returning...",3)
        return False

    # Make sure directory can be made first
    directory = str(outputDir + os.sep + user)
    try:
        log.log("Attempting to make directories for " + directory,0)
        os.makedirs(directory,exist_ok=True)
    except OSError:
        log.log("Could not create parent directories " + directory + "! Returning...",3)
        return False

    # Setup options for youtube_dl
    tag = str(directory + os.sep + ytdlFormat)
    ydl_opts = {
        'outtmpl': tag,
        'logger': SimpleLogger(),
    }

    # Now, try to download given link
    log.log("Attempting to download " + link + "...", 1)
    try:
        with youtube_dl.YoutubeDL(ydl_opts) as ydl:
            ydl.download([link])
    except youtube_dl.DownloadError:
        log.log("Could not download link " + link + "! Returning...",3)
        return False

    return True

# Return string of bool
def boolToString(b: bool):
    if b == True:
        return "True"
    else:
        return "False"

# Returns an array of links in given email message
def getLinks(msg):
    # Credit where credit is due, Python is not my primary language lol
    # https://www.tutorialspoint.com/python_text_processing/python_extract_url_from_text.html
    #urls = []
    #for line in msg:
    #    print("msg is: " + msg)
    #    urlfound = re.findall(r'https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', line)
    #    for url in urlfound:
    #        urls.append(url)
    #        print("I found " + url)
    #return urls
    return re.findall(r'((https?):((//)|(\\\\))+([\w\d:#@%/;$()~_?\+-=\\\.&](#!)?)*)', msg)

# Sends a random response based on the return value
def respond(link, to, rval, pw):
    if sendResponse is False:
        log.log("Not sending response based on config!",0)
        return True
    
    # List of possible reply messages
    positive = [ 
        'Gotchu bb. $link is downloaded!',
        'Successfully downloaded $link!',
        'You know I got that $link downloaded :3',
        'Failure... jk, $link is downloaded lol',
        'Saved $link for you boss!',
        "I hath saved thine $link, m'lady",
        ]
    negative = [
        "Couldn't download $link, sorry man!",
        "Soo... about $link.... nah.",
        "My bad, I can't get ahold of $link",
        "Unable to download $link!",
        "Sorry bud, $link is nowhere to be found. =(",
    ]

    # Pick a random corresponding string and replace with the value
    if rval is True:
        body = random.choice(positive)
    else:
        body = random.choice(negative)
        logFail(link)
    body = body.replace('$link',link)

    # Construct the email
    sender = str(imapUser + "@" + serverAddress)
    msg = email.message.EmailMessage()
    msg.set_content(body)
    msg['Subject'] = 'RE: ' + link
    msg['From'] = sender
    msg['To'] = to

    # Connect and login to server
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    s = smtplib.SMTP(serverAddress,587)
    s.ehlo()
    s.starttls(context=context)
    s.ehlo()
    try:
        s.login(sender,pw)
    except smtplib.SMTPException: # TODO: Code in additional exceptions for more vebose logging
        print("Could not send response about " + link + " to " + to)
        return False
    
    # Send the message
    try:    
        s.send_message(msg)
    except:
        print('Error occurred while sending message to ' + to + ' about ' + link)
        s.quit()
        return False
    
    # Done, quit and return
    s.quit()
    return True

# Logs the failed link to $failLog
def logFail(link: str):
    f = open(failLog,"a")
    f.write(link + '\n')
    f.close()
    return True

### Main

# First, process arguments
parser = argparse.ArgumentParser(description='Check an IMAP server for new emails and download the links within')
parser.add_argument('-s', '--server', dest='address', help='Full address for the IMAP server', type=str, default=None)
parser.add_argument('-d', '--dns-server', dest='dnsServer', help='DNS name to use for connection, if necessary', type=str, default=None)
parser.add_argument('-u', '--user', dest='user', help='Username to connect to the IMAP server with', type=str, default=None)
parser.add_argument('-p','--password',dest='password', help='Password for the IMAP server', type=str, default=None)
parser.add_argument('-v', '--verbose', dest='verbose', help='Enable verbose logging to console', default=False, action='store_true')
parser.add_argument('-vv', '--vverbose', dest='vverbose', help="Enables dev debug logging in addition to verbose", default=False, action='store_true')
parser.add_argument('-r','--read-only', dest='readonly', help='Opens the IMAP inbox in read-only mode (for debugging).', default=False, action='store_true')
parser.add_argument('-m', '--message', dest='message', help='Enables sending of status responses for each email processed', default=False, action='store_true')
parser.add_argument('outputDirectory', help='Directory where youtube-dl will store files', type=str, default=None)
try:
    args = parser.parse_args()
    log.log("Arguments were successfully parsed!",0)
except argparse.ArgumentError:
    log.log("Could not parse arguments! Exiting...",4)
    exit(1)

# Set arguments if present
if args.verbose == True and args.vverbose == False:
    log.printLevel = 1
    log.log("Enabled verbose console logging!",1)
elif args.vverbose == True:
    log.printLevel = 0
    log.logLevel = 0
    log.log("Enabled dev debug logging!",0)
if args.address is not None:
    serverAddress = args.address
    log.log('Using ' + serverAddress + ' for the server address!',1)
if args.user is not None:
    imapUser = args.user
    log.log('Using ' + imapUser + ' as the user!',1)
if args.outputDirectory is not None:
    outputDir = args.outputDirectory
    log.log('Using ' + outputDir + ' as output folder!',1)
if args.readonly is True:
    readOnly = True
    log.log('Turning on read-only mode for the IMAP inbox!',1)
if args.message is True:
    sendResponse = True
    log.log('Enabled sending of response messages!',1)
mailServer = args.dnsServer

# Get the password
fullUser = imapUser + '@' + serverAddress
if args.password is not None:
    tmpPass = args.password
    imapPass = args.password
    log.log('User supplied password at command line!',1)
else:
    tmpPass = 'unsafepassword'
    log.log('Prompting user for password...',0)
    while tmpPass != imapPass:
        imapPass = getpass.getpass(prompt='Enter the password: ')
        tmpPass = getpass.getpass(prompt='Enter password again: ')
        if tmpPass != imapPass:
            log.log("Passwords do not match, looping!",0)

# Now, attempt to login with given credentials
log.log("Attempting to connect to " + serverAddress + " as " + fullUser,0)
if mailServer == None:
    mail = imaplib.IMAP4_SSL(serverAddress)
else:
    mail = imaplib.IMAP4_SSL(mailServer)

try:
    mail.login(fullUser,imapPass)
    log.log("Successfully logged in with given credentials!",1)
except imaplib.IMAP4_SSL.error:
    log.log('Could not connect to server with given credentials! Exiting...',4)
    exit(1)

# Try to grab new emails
try:
    log.log("Searching mailbox INBOX for unread messages...",0)
    mail.select('INBOX',readonly=readOnly)
    typ, messages = mail.search(None,'(UNSEEN)')
    #log.log("Found " + len(messages) + " messages to process!",1)
    for num in messages[0].split():
        # Grab a single message
        #log.log("Working on letter " + num + "...",0)
        typ, letter = mail.fetch(num,'(RFC822)')
        message = email.message_from_bytes(letter[0][1])
        
        # Get username for storing and responding
        uname = re.findall(r'[\w\.-]+@[\w\.-]+', message.get('From'))

        # Now, search each part of the message for a link to download
        for part in message.walk():
            if part.get_content_type() == 'text/plain':
                body = part.get_payload().rstrip()
                
                if body: # Basically 'if not empty'
                    links = getLinks(body)
                    for link in links:
                        log.log("Found link " + link[0] + ", sending to youtube_dl!",0)
                        rtn = ytdl(link[0],uname[0])
                        log.log("youtube_dl returned " + boolToString(rtn) + " from last link!",0)
                        respond(body,uname,rtn,imapPass)
        
        # Mark message as read, then loop
        #typ, letter = mail.store(num,'-FLAGS','\\SEEN')
except imaplib.IMAP4_SSL.error:
    log.log('Could not find any unread messages! Exiting...',2)

mail.logout()
log.log("Done processing emails!",1)
exit(0)