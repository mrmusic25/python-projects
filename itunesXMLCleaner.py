#!/usr/bin/env python3
#
# itunesXMLCleaner.py - Program to clean up a copy of the iTunes XML fils for uploading
#      Makes life easier for uploading to TuneMyMusic
#
# v0.0.4, 25 May 2021, 23:56 PDT

import pathlib
import argparse

# When I looked at my iTunes XML file, these were the tags I deemed unnecessary
# Add or delete tags as necessary here
tags = [
    "Date Modified",
    "Date Added",
    "Disc Number",
    "Disc Count",
    "Persistent ID",
    "Track Type",
    "File Folder Count",
    "Library Folder Count",
    "<key>Size",
    "Bit Rate",
    "Sample Rate",
    "Artwork Count",
    "Purchased",
    "Sort Name",
    "Sort Album",
    "Sort Artist",
    "Sort Album Artist",
    "Sort Composer"
]

### Functions

# True if line contains a tag from above, False otherwise
def contains(line: str):
    for tag in tags:
        if line.find(tag) != -1:
            return True
    return False

def listtostr(l: list):
    s = str()
    for item in l:
        s += str(item) + "\n"
    
    return s

### Main

# Get the XML as an argument, print if not available
parser = argparse.ArgumentParser(description="Program to remove unneccary tags from the iTunes XML file")
parser.add_argument('xmlFile',help="XML file to be processed",default=None,type=str)
try:
    args = parser.parse_args()
    inFile = args.xmlFile
except:
    parser.print_help()
    exit(1)

# Stores the file in current directory
outFile = pathlib.PurePath('.')
outFile = outFile.joinpath('itunesOutput.xml')

# Remove old file if it exists
if pathlib.Path(outFile).exists() == True:
    print("Removing old output file...")
    pathlib.Path(outFile).unlink()

# Read file into variable
lines = list()
with open(str(inFile)) as f:
    lines = f.read().splitlines()
    f.close()

# Copt necessary lines into new variable
outLines = list()
for line in lines:
    if contains(line) == False:
        outLines.append(line)

# Finally, write remaining lines to file
with open(str(outFile),'w') as o:
    o.write(listtostr(outLines))
    o.close()

print("Done with program, edited XML located at: " + str(outFile))
exit(0)