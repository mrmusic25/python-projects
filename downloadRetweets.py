#!/usr/bin/env python
#
# downloadRetweets.py - A program to download everything you have retweeted that has media
#
# v0.1.0, 11 Dec. 2020 17:49 PST

### Imports
import os
import tweepy
import argparse
import webbrowser # Used to open web browser to Twitter if 
from pathlib import Path # Used for getting home directory

### Vars

homePath = str(Path.home()) # Cross-platform implementation of $HOME
defaultSettingsLocation = os.path.join(homePath,str(".downloadRetweets.conf")) # Default config location
defaultFolderLocation = os.path.join(homePath,str("downloadRetweets")) # Default folder where media will be stored
isSetup = True

# These are needed for Twitter API access. Defaults to None for error checking, put up here for global.
CONSUMER_TOKEN = None
CONSUMER_SECRET = None
ACCESS_TOKEN = None
ACCESS_TOKEN_SECRET = None

### Functions

def verifyInfo():
    """Make sure OAuth2 info for Twitter is present"""
    if CONSUMER_TOKEN is None or CONSUMER_SECRET is None or ACCESS_TOKEN is None or ACCESS_TOKEN_SECRET is None:
        isSetup = False
    else:
        isSetup = True

def setup(force = False):
    """Walks user through getting Twitter API token setup and stores in given location"""
    verifyInfo()
    if force is False and isSetup is True:
        return 0

    # Begin setup process if user makes it this far

    webbrowser

### Main

# Start by parsing arguments
parser = argparse.ArgumentParser(description='Download all retweets that contain media')
parser.add_argument('-s', '--setup', dest="doRunSetup", help='Run setup mode then continue with given options', default=False, action="store_true")
parser.add_argument('-l', '--login-info', dest="loginInfo", help='Location where the config is located, defaults to $HOME/.downloadRetweets.conf if empty. Setup will be run if non-existant.', type=str, default=defaultSettingsLocation)
parser.add_argument('-u', '--user', dest="user", help='User to try and grab retweets from. Default to authenticated user otherwise', type=str, default="NONE")
parser.add_argument('outputFolder', help='Folder where all media will be stored. Defaults to $HOME/downloadRetweets', type=str, default=defaultFolderLocation)
args = parser.parse_args()

#print("homePath = " + homePath + ", setup = " + str(args.doRunSetup) + ", login info = " + args.loginInfo + ", user = " + args.user + ", outputFolder = " + args.outputFolder)

exit(0)