#!/usr/bin/env python3
#
# itunesExporter.py - Python program to export iTunes library to .m3u from the XML using libpytunes
#
# Usage: ./itunesExporter.py [-l|--localized] <iTunes_XML_File> <output_Folder>
#   All files will be output to the output_folder
#   If --localized is enabled, filenames relative to media folder will be exported
#     i.e: "F:/iTunes/iTunes Media/Music/Muse/2nd Law/02 Madness.mp3" will become "Muse/2nd Law/02 Madness.mp3", useful for sharing libraries over a file server
#
# v0.2.0, 30 June 2021, 16:24 PDT
from libpytunes import Library # Needed for working with iTunes xml library
import os
import argparse


### Prep Work
parser = argparse.ArgumentParser(description='Export all playlists from iTunes XML library to .m3u8 files')
parser.add_argument('libraryLocation', metavar='L', help='Full path to the iTunes Library.xml file')
parser.add_argument('prefix', metavar='O', help='Output folder for the playlists')
parser.add_argument('-l','--localized', dest='localized', help='Whether or not to localize playlist names to the Music folder', default=None, action='store_true')
args = parser.parse_args()

libraryLocation = args.libraryLocation
l = Library(libraryLocation)
prefix = args.prefix

if args.localized is not None:
    print("Localizing files on export!")
    localize = True
else:
    localize = False

# Make sure prefix ends with an os.sep
if prefix.endswith(os.sep) == False:
    prefix = prefix + os.sep

playlists = l.getPlaylistNames()

i = 2
j = len(playlists)

### Functions 

# Returns location of nth instance of char
def getInstance(s: str, c: str, z: int, reverse=False):
    if reverse == True:
        a = s[::-1]
    else:
        a = s
    
    num = 0
    i = 0
    j = 0

    while i < len(a) and j < z:
        if a[i] == c[0]:
            num = i
            j += 1
        i += 1

    if j == z:
        if reverse == True:
            #print("Char " + c[0] + " num " + str(z) + " of " + a + " is: " + str(j - num - 1))
            return j - num - 3 # Not sure why -3 is needed, but it works
        else:
            return num
    else:
        return -1
        

def getSongName(s):
    name = s.name
    if name is None:
        return "Title"
    else:
        return name

def getSongArtist(s):
    name = s.artist
    if name is None:
        return "Artist"
    else:
        return name

def getSongTime(s):
    time = s.length
    if not isinstance(time, int) or time is None:
        return "0"
    else:
        time = int(round(time / 1000))
        return str(time)

def getSongLocation(s, local = False):
    l = s.location
    if l == "" or l is None:
        return "#LOCATION NOT FOUND"
    else:
        if local == True:
            try:
                i = getInstance(l,'/',3,reverse=True)
                j = len(l)
                return l[i:j]
            except:
                return l
        return l

### Main

while i < j:
    if l.getPlaylist(playlists[i]).is_folder:
        print("Skipping folder " + playlists[0] + "!")
    else:
        outputFile = prefix + playlists[i] + ".m3u8"
        print("Outputting playlist " + outputFile + "!")
        
        # First, make sure file does not exist, and replace if so
        if os.path.exists(outputFile):
            print("Removing original " + playlists[i] + ".m3u...")
            os.remove(outputFile)
        f = open(outputFile, "a")
        
        # Start writing the default M3U header
        f.write("#EXTM3U")
        
        # Now, output each song's info and location
        for song in l.getPlaylist(playlists[i]).tracks:
            minfo = "\n#EXTINF:" + getSongTime(song) + "," + getSongArtist(song) + " - " + getSongName(song) + '\n'
            f.write(minfo)
            f.write(str(getSongLocation(song,localize)))
        f.close()
    i = i + 1