#!/usr/bin/env python
# getPlaylists.py - A tool to download all playlists from Google Play Music and export to text
from gmusicapi import Mobileclient
import sys
import os
import time
import argparse

### Functions

def findTrack(songs,trackID):
    for song in songs:
        if song['id'] == trackID:
            return song
    return str("NULL")

def outputPlaylist(api,tracks,outFile,songs):
    if os.path.exists(outFile):
        print("Removing previous file " + outFile + "...")
        os.remove(outFile)

    f = open(outFile, "a")
    for track in tracks:
        song = findTrack(songs,track['trackId'])
        if song == "NULL":
            print("Unable to find song with trackID: " + str(track['id']))
        else:
            title = song['title']
            artist = song['artist']
            album = song['album']
            outStr = str(artist + '|' + album + '|' + title + '\n')
            f.write(outStr)
    f.close()

def cleanName(pname):
    tmp = ""
    for ele in pname:
        if ele == '/':
            tmp = tmp + '-'
        else:
            tmp = tmp + ele
    return tmp
        

### Main

# Get arguments
parser = argparse.ArgumentParser(description="Import playlists from GPM using gmusicapi")
parser.add_argument('outputFolder',metavar='O', help="Directory where all playlists will be stored")
args = parser.parse_args()

outFolder = args.outputFolder

if not outFolder.endswith(os.sep):
    outFolder = outFolder + os.sep

# Setup the gmusicapi
api = Mobileclient()
api.__init__()


# Check to see if OAuth credentials available, ask user to setup if not
try:
    if not api.oauth_login(Mobileclient.FROM_MAC_ADDRESS):
        print("No OAuth credentials found! Please setup in the following screen!")
        api.perform_oauth()
        api.oauth_login(Mobileclient.FROM_MAC_ADDRESS)
except:
    print("No OAuth credentials found! Please setup in the following screen!")
    api.perform_oauth()
    api.oauth_login(Mobileclient.FROM_MAC_ADDRESS) # If it fails here, it wasn't meant to be

# Then, move on to doing all the work
if api.is_authenticated():
    print("Successfully logged in. Outputting playlist contents to text.")
    print("Now downloading playlist data...")
    playlists = api.get_all_user_playlist_contents()
    print("Downloading list of all songs...")
    songs = api.get_all_songs()

    for playlist in playlists:
        pname = str(outFolder + cleanName(playlist['name']) + ".txt")
        tracks = playlist['tracks']
        print("Outputting contents of " + playlist['name'] + " to: " + pname)
        outputPlaylist(api,tracks,pname,songs)
    api.logout()
else:
    print("Not logged in! Exiting...")
    exit(1)

print("Script has finished successfully!")