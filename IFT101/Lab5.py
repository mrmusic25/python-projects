#!/usr/bin/env python
#
# Name: Kyle Krattiger
# Class: IFT 101
# Purpose: (Part 1) Convert algorithm from assignment 5 to running code
#          (Part 2) Write a function that prints the multiplication table of a given number
# v1.0.0

# Part 1
import random

# 0 is heads, 1 is tails
def flipCoin():
    return random.randint(0,1)

flips = int( input("Enter how many times you would like to flip the coin: ") )
counter = 0
heads = 0
tails = 0
while counter < flips:
    if flipCoin() == 0:
        heads = heads + 1
    else:
        tails = tails + 1
    counter = counter + 1

print("Number of heads: " + str(heads) + ", number of tails: " + str(tails)) 

# Part 2

def multiply(n):
    counter = 1
    while counter <= 10:
        print(str(n) + " x " + str(counter) + " = " + str(counter*n))
        counter = counter + 1

num = int( input("Enter the number you would like to see times tables for: ") )
multiply(num)

#EOF