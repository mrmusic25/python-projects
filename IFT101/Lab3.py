#!/usr/bin/env python
#
# Name: Kyle Krattiger
# Class: IFT 101
# Purpose: (Part 1) Average three numbers
#          (part 2) determine if given number is even or odd
# v1.0.0
import math

# Part 1

num1 = int(input("Enter the first number: "))
num2 = int(input("Enter the second number: "))
num3 = int(input("Enter the third number: "))
average = ( num1 + num2 + num3 ) / 3

print("Average of the three numbers is: " + str(average))

# Part 2

eonum = int(input("Enter an integer value: "))

if eonum % 2 == 0:
    print(str(eonum) + " is an even number.")
else:
    print(str(eonum) + " is an odd number.")

#EOF