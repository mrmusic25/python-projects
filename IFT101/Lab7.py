#!/usr/bin/env python
#
# Name: Kyle Krattiger
# Class: IFT 101
# Purpose: (Part 1) Output the given data to the desired format 
#          (Part 2) Code the solution to Assignment 7
#
# v1.0.0

### Part 1
filename = open("EmployeeInfo.txt",'w')

oline = str("Kelley1H01232") # Padding for payrate is a starting zero
filename.write(oline + "\n")

oline = str("Jason~2S70040") # Using trailing '~' as a pad character
filename.write(oline + "\n")

oline = str("Alica~3S72040")
filename.write(oline + "\n")

oline = str("Debra~4H01319")
filename.write(oline + "\n")

oline = str("Gordon5H01223")
filename.write(oline + "\n")

filename.close()

### Part 2
filename = open("EmployeeInfo.txt",'r')

for line in filename:
    name = line[0:6] # Turns out when using substring in Python you need the end to be one higher than the actual endpoint
    while name[len(name)-1] == '~':
        name = name[0:len(name)-1] # Effectively deletes the last char if it is a '~', the padding space
    employeeID = line[6]
    payType = line[7]
    payRate = line[8:11]
    hoursWorked = line[11:13]

    if payType == 'H':
        grossPay = int(payRate) * int(hoursWorked)
    else:
        grossPay = int(payRate)

    print("\n")
    print("Name: " + name)
    print("ID: " + employeeID)
    print("Pay type: Hourly" if payType == 'H' else "Pay type: Salaried") # Ternary in python is suprisingly simple
    print("Pay rate: $" + payRate)
    print("Hours worked: " + hoursWorked)
    print("Gross pay: $" + str(grossPay))

filename.close()
#EOF