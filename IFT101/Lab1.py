#!/usr/bin/env python3

# Name: Kyle Krattiger
# Class: IFT 101
# Purpose: Demonstrate working Python
# v1.0.0

# Part 1
print("Hello!")
print("This is my first python program!")

# Part 2
x = 10
print(x)

y = 15
print(y)

z = 0.7
print(z)

course_name = "Introduction to Programming Logic"
print(course_name)

a = True
b = False

# Part 3
sum = x + y
print(sum)

print(x > y)
print(x != y)

print(a and b)
print(a or b)

na = not a
print(na)

#EOF