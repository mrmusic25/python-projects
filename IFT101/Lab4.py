#!/usr/bin/env python
#
# Name: Kyle Krattiger
# Class: IFT 101
# Purpose: (Part 1) Write code from the pseudocode in Assignment 4
#          (Part 2) Calculate pay based on hours and type of pay
# v1.0.0

# Part 1
grade = int(input("Please enter the percent you recieved on assignment 1: "))

if grade <= 100 and grade >= 90:
    letterGrade = 'A'
elif grade >= 80:
    letterGrade = 'B'
elif grade >= 70:
    letterGrade = 'C'
elif grade >= 60:
    letterGrade = 'D'
else: 
    letterGrade = 'F'

print("Grade received: " + letterGrade)

grade = int(input("Please enter the percent you received on assignment 2: "))
import math

def lGrade(i):
    x = int(math.floor(i/10))
    switcher={
        10:'A',
        9:'A',
        8:'B',
        7:'C',
        6:'D'
    }
    return switcher.get(x,'F')

letterGrade = lGrade(grade)

print("Grade received: " + letterGrade)

# Part 2
hourlyRate = 15
overtimeRate = 20
salaryRate = 500
choice = 0

hours = int(input("Please enter the number of hours worker this week: "))

while choice != 1 and choice != 2:
    choice = int(input("Please enter 1 for salaried, 2 for hourly: "))

if choice == 1:
    pay = hours * salaryRate
else:
    if hours > 40:
        ot = hours - 40
        hours -= ot
        pay = hours * hourlyRate + (overtimeRate * ot)
    else:
        pay = hours * hourlyRate

print("Pay for this week: $" + str(pay))

#EOF