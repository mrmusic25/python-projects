#!/usr/bin/env python3

# Name: Kyle Krattiger
# Class: IFT 101
# Purpose: (Part 1) Print the highest of three given numbers, and (part 2) calculate number of boxes needed
# v1.0.0
import math

# Part 1
number1 = int( input("Enter the first number: ") )
number2 = int( input("Enter the second number: ") )
number3 = int( input("Enter the third number: ") )

largest = -1e10 # Negative 10 billion

if number1 >= number2 and number1 >= number3:
    largest = number1
elif number2 >= number1 and number2 >= number3:
    largest = number2
else:
    largest = number3

print(str(largest) + " was the largest of the three numbers.")

# Part 2
n_reps = 100
n_extra_reps = 5
n_pages = 20
n_papers_box = 200
n_req_papers = (n_reps + n_extra_reps) * n_pages
n_req_boxes = math.ceil(n_req_papers / n_papers_box)

print("The number of required paper boxes is: " + str(n_req_boxes))

#EOF