#!/usr/bin/env python
#
# Name: Kyle Krattiger
# Class: IFT 101
# Purpose: (Part 1) Convert algorithm from assignment 6 to running code
#          (Part 2) 
# v1.0.0

### Part 1

# Return index of the max number
def findMax(a):
    index = 0
    max = 0
    while index < len(a):
        if a[index] > a[max]:
            max = index
        index += 1
    return max

# Return index of the min number
def findMin(a):
    index = 0
    min = 0
    while index < len(a):
        if a[index] < a[min]:
            min = index
        index += 1
    return min

## Main
i = 0
vals = dict()
while i < 14:
    ask = int(input("Enter the highest windspeed on day " + str(i+1) + ": "))
    vals[i] = ask
    i += 1

max = findMax(vals)
min = findMin(vals)
diff = vals[max] - vals[min]

print("Highest windspeed was on day " + str(max+1) + ", lowest on day " + str(min+1) + "; differece between two speeds is " + str(diff))

### Part 2

# Question 1
def string_times(msg,n):
    i = 0
    while i <= n:
        print(msg)
        i += 1

# Question 2
names = dict()
onamae = str()
j = 0
while True:
    onamae = str(input("Please enter a name, or enter 0 to quit: "))
    if onamae == str(0):
        break
    else:
        names[j] = onamae
    j += 1

print("Names entered: ")
z = 0
while z < len(names):
    print(names[z])
    z += 1

# Question 3
values = dict()
x = 0
while x < 5:
    values[x] = int(input("Please enter a number: "))
    x += 1

print("Reversed values:")
while x > 0:
    print(values[x-1])
    x -= 1

#EOF