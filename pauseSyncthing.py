#!/usr/bin/python3
#
# pauseSyncthing.py - Simple script to pause/unpause Syncthing using REST API
#
# Usage:
#   ./pauseSyncthing.py [options] <folderID> <stop|start>
#
#     where folderID is the ID found in the specific folder under the Web GUI in Edit -> Folder ID
#     Run './pauseSyncthing.py --help' for additional info.
#
# v0.2.1, Kyle Krattiger, 07 Aug. 2022 17:35 PDT

### Imports

from argparse import ArgumentError, ArgumentParser
import pathlib
import json
import requests

### Globals

HOME = pathlib.Path().home()                   # User's home folder

### Variables

proto = "https"                                 # Protocol to use. Depending on settings can be http, or https.
hostname = "localhost"                          # Hostname to use. Can be localhost or 127.0.0.1, or FQDN if Let's Encrypt certificates are used.
port = "8384"                                   # Port where the Syncthing Web GUI is hosted on
#baseURL = "https://127.0.0.1:8384"             # Server IP and port to use. localhost by default
apiFolder = "rest/config/folders"               # Folder path to where folder info is stored
apiKey = ""                                     # API key used for transactions. Found in (Web GUI) -> Actions -> Advanced -> GUI -> Api Key. Saved/loaded from apiKey below.
apiFile = HOME.joinpath(".syncthing-key.txt")   # Location of the API key file
hostFile = HOME.joinpath(".syncthing-host.txt") # Location of the hostname file
folderID = ""                                   # Syncthing ID of the folder to be edited
action = ""                                     # Action to be performed on the folderID

### Functions

def save():
    """Saves the API key to the file specified by apiFile. Returns True on success, False otherwise."""
    if not apiKey or apiKey == "":
        return False

    try:
        with open(str(apiFile),'w') as f:
            f.write(apiKey)
            f.close()
        return True
    except:
        return False

def saveHost():
    """Saves the current hostname to the file specified by hostnameFile. Returns True on success, False otherwise."""
    if not hostname or hostname == "":
        return False

    try:
        with open(str(hostFile),'w') as f:
            f.write(hostname)
            f.close()
        return True
    except:
        return False

def load():
    """Loads the API key from the file specified by apiFile. Returns True on success, False otherwise."""
    global apiKey

    if not apiFile.exists():
        return False

    try:
        with open(str(apiFile),'r') as f:
            rtn = f.read()
            f.close()
    except:
        return False

    apiKey = rtn.strip()
    return True

def run():
    """Runs the given action on the REST API for given folderID. Returns True on success (based on HTTP return value) and False on errors."""
    # Setup the necessary variables
    url = proto + "://" + hostname + ":" + str(port) + '/' + apiFolder + "/" + folderID
    header = {
        'Accept': 'application/json',
        'X-API-Key': apiKey
    }
    data = {'paused': action} # Shoutout to: https://stackoverflow.com/a/53078535

    # Run thew request and report
    req = requests.patch(url,data=json.dumps(data),headers=header)

    if not isinstance(req,requests.Response):
        return False
    elif req.status_code > 299:
        print("Fail reason: " + str(req.status_code) + " " + req.reason)
        return False
    else:
        return True

### Main

if __name__ == "__main__":
    # Setup the argument parser
    parser = ArgumentParser(usage="A simple program to pause/unpause a Syncthing folder using the REST API")
    parser.add_argument("folderID",help="Folder ID for the folder to be edited. Required.",default=None,type=str)
    parser.add_argument("action",help="What action to perform on this folder. Can be start/stop, on/off, or pause/unpause. Required.",default=None,type=str)
    parser.add_argument('-k','--api-key',help='The API key from your Syncthing session. Program will fail if not already in apiFile. Will be stored there, so only need to include this option once.',type=str,default=None)
    parser.add_argument('-f','--api-file',help='File to store/load the API key from. $HOME/.syncthing-key.txt by default.',default=None,type=str)
    parser.add_argument('-n','--hostname',help='Specify the hostname/IP address to use for connections. Useful for Let\'s Encrypt certificates.',default=None,type=str)

    try:
        args = parser.parse_args()
    except ArgumentError:
        print("Could not read arguments! Exiting...")
        parser.print_usage()
        exit(1)

    # Import the arguments
    if args.api_file is not None:
        if pathlib.Path(args.api_file).is_absolute():
            apiFile = pathlib.Path(args.api_file)
            print("Using absolute path " + str(apiFile) + " for the storing/loading of the API key file!")
        else:
            apiFile = HOME.joinpath(args.api_file)
            print("Using " + str(apiFile) + " for storing/loading of the API key file!")
    
    if args.api_key is not None:
        apiKey = args.api_key
        if not save():
            print("Could not save API key to " + str(apiFile) + ", does user have permissions? Continuing...")
    else:
        if not load():
            print("Could not load API key and none was given! Cannot access REST API, exiting!")
            exit(1)
    
    if args.hostname is not None:
        hostname = args.hostname
        print("Saving new hostname '" + hostname + "' to hostname file...")
        if not saveHost():
            print("Could not save new hostname to hostFile " + str(hostFile) + ", does user have permission? Attempting to continue...")
    elif hostFile.exists():
        with open(str(hostFile),'r') as f:
            hostname = f.read().strip()
            f.close()
        print("Hostname is set to '" + hostname + " from file!")

    if args.folderID is None:
        print("Folder ID was not given, cannot continue! Please fix and re-run! Exiting...")
        parser.print_usage()
        exit(1)
    else:
        folderID = args.folderID
    
    if args.action is None:
        print("No action was specified, cannot continue! Please fix and re-run! Exiting...")
        parser.print_usage()
        exit(1)
    else:
        if args.action.lower().strip() in [ 'on', 'unpause', 'start', '1', 'enable', 'go', 'restart' ]:
            action = False # Disable pausing, opposite boolean value
        elif args.action.lower().strip() in [ 'off', 'pause', 'stop', '0', 'disable', 'halt' ]:
            action = True

    # Now, run the action
    if not run():
        print("Could not change the status! See reason above for more info! Exiting...")
        exit(1)
    else:
        print("Changed status successfully! Exiting...")
        exit(0)

#EOF